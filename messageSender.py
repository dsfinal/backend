import datetime
import re
import calendar
import json
import time
import pika

brokerHost  = 'crow.rmq.cloudamqp.com'
port        = 5672
username    = 'zhdwvmjr'
password    = 'd2LlWdDSAtCVCz7zXz7zBtqEGejD5bxn'
exchange    = 'sensorData'
patientId = '7';
def readOneLine():
    with open('activity.txt') as f:
        while True:
            data = f.readline()
            if data == '':
                break
            yield data


def getPayloadFromEventLine(line):
    parts = re.split(r'\t+', line)[:-1]
    return {
        'patientId':    patientId,  
        'activity':     parts[2],
        'start':        calendar.timegm(datetime.datetime.strptime(parts[0], '%Y-%m-%d %H:%M:%S').timetuple()),
        'end':          calendar.timegm(datetime.datetime.strptime(parts[1], '%Y-%m-%d %H:%M:%S').timetuple()),
        'forDb':        str(parts[0]) +'  '+ str(parts[1]) + '  ' + str(parts[2])  
    }


def sendNextEvent(reader, channel):
    try:
        event = getPayloadFromEventLine(next(reader))
        sendPayload(json.dumps(event), channel)

    except StopIteration:
        #daca da de capat le ia de la anceput
        reader = readOneLine()


def sendPayload(payload, channel):
    channel.basic_publish(
        exchange=exchange, 
        routing_key='messageQueue',
        body=payload
    )
    print(payload)


def main():
    reader = {}
    reader = readOneLine()

    credentials = pika.PlainCredentials(username, password)
    parameters = pika.ConnectionParameters(brokerHost,
                                       port,
                                       username,
                                       credentials)
    with pika.BlockingConnection(parameters) as connection:
        while 1:
            command = input('~> ');
            if command == 'send':
                channel = connection.channel()
                channel.exchange_declare(exchange=exchange, exchange_type='fanout')
                sendNextEvent(reader,channel);
            
            elif command == 'exit':
                break;

            elif command == 'testToilet':
                channel = connection.channel()
                channel.exchange_declare(exchange=exchange, exchange_type='fanout')
                sendPayload(json.dumps({'patientId': patientId, 'activity': 'Toileting', 'start': '0', 'end': '1801', 'forDb': 'Toileting  startTest  endTest'}),channel)

            elif command == 'testLeaving':
                channel = connection.channel()
                channel.exchange_declare(exchange=exchange, exchange_type='fanout')
                sendPayload(json.dumps({'patientId': patientId, 'activity': 'Leaving', 'start': '0', 'end': '18001', 'forDb': 'Leaving  startTest  endTest'}),channel)

            elif command == 'testSleeping':
                channel = connection.channel()
                channel.exchange_declare(exchange=exchange, exchange_type='fanout')
                sendPayload(json.dumps({'patientId': patientId, 'activity': 'Sleeping', 'start': '0', 'end': '25201', 'forDb': 'Sleeping  startTest  endTest'}),channel)

            else:
                print('exit to exit, send to send, testToilet to testToilet, testLeaving to testLeaving, testSleeping to testSleeping')


if __name__ == "__main__":
        main()