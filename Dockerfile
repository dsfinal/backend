FROM maven:3.6.3-jdk-11 AS builder

COPY ./src/ /root/src
COPY ./pom.xml /root/
WORKDIR /root
RUN mvn package -Dmaven.test.skip
RUN java -Djarmode=layertools -jar /root/target/back-0.0.1-SNAPSHOT.jar list
RUN java -Djarmode=layertools -jar /root/target/back-0.0.1-SNAPSHOT.jar extract
RUN ls -l /root

FROM openjdk:11-jre

ENV TZ=UTC
ENV DB_IP=ec2-54-247-158-179.eu-west-1.compute.amazonaws.com
ENV DB_PORT=5432
ENV DB_USER=hectpmifzsnkck
ENV DB_PASSWORD=6bc7138ec6be16e5c7c158044b3c946dd2fcd6fa68789184db83132768b765ed
ENV DB_DBNAME=d37lo8m0ka4o5d

COPY --from=builder /root/dependencies/ ./
COPY --from=builder /root/snapshot-dependencies/ ./

RUN sleep 10
COPY --from=builder /root/spring-boot-loader/ ./
COPY --from=builder /root/application/ ./
ENTRYPOINT ["java", "org.springframework.boot.loader.JarLauncher","-XX:+UseContainerSupport -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=1 -Xms128m -Xmx128m -XX:+UseG1GC -XX:+UseSerialGC -Xss512k -XX:MaxRAM=72m"]

