package com.DS.back.controller;

import com.DS.back.model.*;
import com.DS.back.repos.CaregiverRepo;
import com.DS.back.repos.DoctorRepo;
import com.DS.back.repos.PatientRepo;
import com.DS.back.DTO.AuthResponseDTO;
import com.DS.back.DTO.UserDTO;
import com.DS.back.security.MyUserDetailsService;
import com.DS.back.util.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.web.bind.annotation.*;

//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "https://dsfinalfront.herokuapp.com")
    @RestController
    @RequestMapping("/api")

    public class AuthController {
    @Autowired
    PatientRepo patientRepo;
    @Autowired
    CaregiverRepo caregiverRepo;
    @Autowired
    DoctorRepo doctorRepo;
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    MyUserDetailsService myUserDetailsService;
    @Autowired
    JwtUtil jwtTokenUtil;

    @PostMapping("/login")
    public AuthResponseDTO authentification(@RequestBody UserDTO userDTO) {
        if (doctorRepo.findAll().size() == 0L) doctorRepo.save(Doctor.builder().name("d").password("d").build());

        Long returnBox = patientRepo.findByNameAndPassword(userDTO.getName(),userDTO.getPassword()).orElse(Patient.builder().idPatient((long)-1).build()).getIdPatient();
        if(returnBox != (long)-1){
            return AuthResponseDTO.builder().userId(returnBox).type("P").build();
        }
        returnBox = doctorRepo.findByNameAndPassword(userDTO.getName(),userDTO.getPassword()).orElse(Doctor.builder().idDoctor((long)-1).build()).getIdDoctor();
        if(returnBox != (long)-1){
            return AuthResponseDTO.builder().userId(returnBox).type("D").build();
        }
        returnBox = caregiverRepo.findByNameAndPassword(userDTO.getName(),userDTO.getPassword()).orElse((Caregiver.builder().idCaregiver((long)-1).build())).getIdCaregiver();
        if(returnBox != (long)-1){
            return AuthResponseDTO.builder().userId(returnBox).type("C").build();
        }
        return AuthResponseDTO.builder().type("F").userId(returnBox).build();
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) throws Exception{
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getUsername(), authenticationRequest.getPassword()));
        }
        catch(BadCredentialsException e){
            throw new Exception("Incorrect user or password",e);
        }
        final UserDetails userDetails = myUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());
        final String jwt = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new AuthenticationResponse(jwt));
    }
}