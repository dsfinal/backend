package com.DS.back.controller;

import com.DS.back.model.Caregiver;
import com.DS.back.model.Patient;
import com.DS.back.repos.CaregiverRepo;
import com.DS.back.repos.PatientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.Caret;
import java.util.ArrayList;
import java.util.List;

@RestController
//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "https://dsfinalfront.herokuapp.com")
@RequestMapping("/api")
public class CaregiverController {
    @Autowired
    CaregiverRepo caregiverRepo;
    @Autowired
    PatientRepo patientRepo;
    @GetMapping("caregivers/r")
    public List<Caregiver> getAllCaregivers(){
        return caregiverRepo.findAll();
    }

    @DeleteMapping("caregivers/d/{id}")
    public ResponseEntity<?> deleteCaregiver(@PathVariable(value = "id") Long id){
        Caregiver toDelete = caregiverRepo.findByIdCaregiver(id).get();
        toDelete.getPatients().forEach(x->x.setCaregiver(null));
        toDelete.getPatients().clear();
        caregiverRepo.deleteById(id);
        return ResponseEntity.ok().build();
    }


    @PostMapping("caregivers/cu")
    public Caregiver createCaregiver(@RequestBody Caregiver caregiver){
        return caregiverRepo.save(caregiver);
    }
    @PostMapping("caregivers/e")
    public Caregiver editCaregiver(@RequestBody Caregiver caregiver){
        List<Patient> currentPatient = patientRepo.findByCaregiver_idCaregiver(caregiver.getIdCaregiver());
        currentPatient.forEach(x->{x.setCaregiver(null);});
        caregiver.getPatients().forEach(x->{x.setCaregiver(caregiver);});
        patientRepo.saveAll(caregiver.getPatients());
        return caregiverRepo.save(caregiver);
    }
}
