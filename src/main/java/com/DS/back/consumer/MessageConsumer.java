package com.DS.back.consumer;

import com.DS.back.model.Patient;
import com.DS.back.repos.PatientRepo;
import com.DS.back.websocket.wSocketEndpoint;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.type.IntegerType;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;

@Component
public class MessageConsumer {
    private final static String QUEUE_NAME = "messageQueue";
    PatientRepo patientRepo;

    public void ActivitiesReader(PatientRepo patientRepo) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        this.patientRepo = patientRepo;
        factory.setUsername("zhdwvmjr");
        factory.setPassword("d2LlWdDSAtCVCz7zXz7zBtqEGejD5bxn");
        factory.setVirtualHost("zhdwvmjr");
        factory.setHost("crow.rmq.cloudamqp.com");
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();

        channel.exchangeDeclare("sensorData","fanout");
        channel.queueDeclare(QUEUE_NAME, false, false, false, null);
        channel.queueBind(QUEUE_NAME,"sensorData",QUEUE_NAME);

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            Gson gson = new Gson();
            JsonObject json = gson.fromJson(new String(delivery.getBody()), JsonObject.class);
            Long duration = json.get("end").getAsLong() - json.get("start").getAsLong();
            switch (json.get("activity").getAsString()) {
                case "Sleeping":
                    if(duration > 25200){
                        if(saveMessage(json))
                        wSocketEndpoint.broadcastMessage("m" + json.get("patientId").getAsString() + " patient" + json.get("patientId").getAsString() + "slept too much");
                    }
                    break;

                case "Toileting":
                    if(duration > 1800){
                        if(saveMessage(json))
                        wSocketEndpoint.broadcastMessage("m" + json.get("patientId").getAsString() + " patient" + json.get("patientId").getAsString() + "stayed too much in the bathroom");
                    }
                    break;

                case "Leaving":
                    if(duration > 18000){
                        if(saveMessage(json))
                        wSocketEndpoint.broadcastMessage("m" + json.get("patientId").getAsString() + " patient" + json.get("patientId").getAsString() + "stayed too much outside");
                    }
                    break;
            }
        };
        channel.basicConsume(QUEUE_NAME, true, deliverCallback, consumerTag -> { });
    }

    private boolean saveMessage(JsonObject json){

        Patient patient = patientRepo.findByIdPatient(json.get("patientId").getAsLong()).orElse(Patient.builder().build());
        if(patient.getIdPatient() == null) {System.out.println("doesn't exist"); return false;}
        String oldRecord = patient.getSensor();
        if(oldRecord == null) oldRecord ="";
        patient.setSensor(oldRecord + "\n" + json.get("forDb"));
        patientRepo.save(patient);
        return true;
    }
}