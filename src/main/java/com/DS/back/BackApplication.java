package com.DS.back;

import com.DS.back.consumer.MessageConsumer;
import com.DS.back.repos.PatientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.security.MessageDigest;

@SpringBootApplication
public class BackApplication {

	public static void main(String[] args) throws Exception { SpringApplication.run(BackApplication.class, args);
	}

}
