package com.DS.back.security;

import com.DS.back.model.Caregiver;
import com.DS.back.model.Doctor;
import com.DS.back.model.Patient;
import com.DS.back.repos.CaregiverRepo;
import com.DS.back.repos.DoctorRepo;
import com.DS.back.repos.PatientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.management.relation.Role;
import java.util.ArrayList;
import java.util.List;

@Service
public class MyUserDetailsService implements UserDetailsService {
    @Autowired
    DoctorRepo doctorRepo;
    @Autowired
    PatientRepo patientRepo;
    @Autowired
    CaregiverRepo caregiverRepo;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        ArrayList<GrantedAuthority> roles = new ArrayList<>();
        Doctor doctor =doctorRepo.findAll().stream().filter(x-> x.getName().equals(username)).findFirst().orElse(null);
        Patient patient = patientRepo.findAll().stream().filter(x->x.getName().equals(username)).findFirst().orElse(null);
        Caregiver caregiver = caregiverRepo.findAll().stream().filter(x->x.getName().equals(username)).findFirst().orElse(null);
        if(doctor != null){

            roles.add(new SimpleGrantedAuthority("D"));
            return new User(doctor.getName(),doctor.getPassword(),roles);
        }
        if(patient != null){
            roles.add(new SimpleGrantedAuthority("P"));
            return new User(patient.getName(),patient.getPassword(),roles);
        }
        if(caregiver != null){
            roles.add(new SimpleGrantedAuthority("C"));
            return new User(caregiver.getName(),caregiver.getPassword(),roles);
        }
        return null;
    }
}
