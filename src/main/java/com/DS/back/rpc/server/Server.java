package com.DS.back.rpc.server;

import com.DS.back.repos.MedInfoRepo;
import com.DS.back.rpc.model.MedPlan;
import com.DS.back.websocket.wSocketEndpoint;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

@Component("chatserver")
public class Server extends UnicastRemoteObject implements ServerIF {
    private static int client_count = 0;

    MedInfoRepo medInfoRepo;

    public Server(MedInfoRepo medInfoRepo) throws RemoteException {
    this.medInfoRepo = medInfoRepo;
    }

    @Override
    public String getPlan() throws RemoteException {
        List<MedPlan> planList = new ArrayList<MedPlan>();
        medInfoRepo.findByPatient_idPatient(7L).forEach(x->{
            planList.add(MedPlan.builder().medicationId(x.getMedication().getIdMedication()).name(x.getMedication().getName()).interval(Duration.ofSeconds(x.getPeriod())).build());
        });
        return new Gson().toJson(planList);
    }

    @Override
    public void takeMedicine(String message) throws RemoteException {
        System.out.println("r7 " + message + " taken");
        wSocketEndpoint.broadcastMessage("r7 " + message + " taken");
    }

    @Override
    public void passMedicine(String message) throws RemoteException {
        System.out.println("r7 " + message + " was not taken !");
        wSocketEndpoint.broadcastMessage("r7 " + message + " was not taken !");
    }
}
