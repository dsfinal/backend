package com.DS.back.rpc.server;


import com.DS.back.consumer.MessageConsumer;
import com.DS.back.repos.MedInfoRepo;
import com.DS.back.repos.PatientRepo;
import com.DS.back.rpc.server.Server;
import com.DS.back.rpc.server.ServerIF;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.caucho.HessianServiceExporter;
import org.springframework.remoting.support.RemoteExporter;

import java.rmi.RemoteException;

@Configuration
public class HessianConfiguration {

    @Autowired
    MedInfoRepo medInfoRepo;
    //cam sarma da' da-i pace acuma'
    @Autowired
    PatientRepo patientRepo;

    @Bean(name="/MedicationData")
    RemoteExporter registerRMIExporter() throws Exception {
        //sarma
        MessageConsumer messageConsumer = new MessageConsumer();

        messageConsumer.ActivitiesReader(patientRepo);
        //sarma

        HessianServiceExporter exporter = new HessianServiceExporter();
        exporter.setServiceInterface(ServerIF.class);
        exporter.setService(new Server(medInfoRepo));

        return exporter;
    }

}