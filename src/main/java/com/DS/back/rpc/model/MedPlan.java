package com.DS.back.rpc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NonNull;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.Duration;
@Builder
@AllArgsConstructor
public class MedPlan implements Serializable {
    private Long medicationId;
    private String name;
    private Duration interval;
}
